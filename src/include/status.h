#ifndef STATUS
#define STATUS

enum read_status {
    READ_OK = 0,
    READ_INVALID_HEADER,
    READ_INVALID_SIGNATURE,
    READ_INVALID_PIXEL,
    READ_INVALID_FORMAT
    READ_INVALID_SIZE,
    READ_ERROR,
};

enum write_status {
    WRITE_OK = 0,
    WRITE_INVALID_FILE,
    WRITE_INVALID_HEADER,
    WRITE_INVALID_BITS

    WRITE_ERROR,
};

enum open_status {
    OPEN_OK = 0,
    OPEN_INVALID_FILE,
    OPEN_ERROR
};

enum close_status{
    CLOSE_OK = 0,
    CLOSE_ERROR,
    CLOSE_ERROR_IN_FILE
};


#endif
