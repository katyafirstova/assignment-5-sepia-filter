#ifndef IMAGE_H
#define IMAGE_H

#include <inttypes.h>
#include <stdbool.h>
#include <stddef.h>


struct image {
    uint32_t width;
    uint32_t height;
    struct pixel *data;
};

struct pixel {
    uint8_t b;
    uint8_t g;
    uint8_t r;
};

struct image image_create(const uint32_t width, const uint32_t height);

void image_destroy(struct image* image);

bool image_set_pixel (struct image image, struct pixel pixel, uint32_t x, uint32_t y);

struct pixel image_get_pixel (struct image image, uint32_t x, uint32_t y);

size_t image_get_height(struct image *image) {
    return image->height;
}

size_t image_get_width(struct image *image) {
    return image->width;
}

void image_set_height(struct image *image, size_t height) {
    image->height = height;
}

void image_set_width(struct image *image, size_t width) {
    image->width = width;
}




#endif 






