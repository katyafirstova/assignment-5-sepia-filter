
#ifndef SEPIA_H
#define SEPIA_H
#include "image.h"

struct image do_sepia_c(struct image img);
struct image do_sepia_asm(struct image img);

#endif
