#ifndef FILE_H
#define FILE_H

#include <stdbool.h>
#include <stdio.h>
#include "status.h"


enum open_status from_file(const char* filename, struct image* img);
enum open_status to_file(const char* filename, struct image* img);
